package com.example.demo;

import java.util.ArrayList;

import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class Task6030Controller {
	private final int CDPLAYER = 1;
	private final int DVDPLAYER = 2;
	private final int TAPEPLAYER = 3;
	
	private final int BASEBALL = 1;
	private final int FOOTBALL = 2;
	

	@CrossOrigin
	@GetMapping("/listPlayer2")

	public ArrayList<Person> getListPlayer(@RequestParam(value = "play", defaultValue = "0") String play,@RequestParam(value = "ballType", defaultValue = "0") String ballType) {
		ArrayList<Person> listPlayer = new ArrayList<Person>();

		ArrayList<Player> listPlay = new ArrayList<Player>();

		CDPlayer myCDPlayer = new CDPlayer();
		DVDPlayer myDVDPlayer = new DVDPlayer();
		TapePlayer myTapePlayer = new TapePlayer();

		listPlay.add(myCDPlayer);
		listPlay.add(myDVDPlayer);
		listPlay.add(myTapePlayer);

		ArrayList<Ball> listBall = new ArrayList<Ball>();
		Baseball myBaseBall = new Baseball("BaseBall");
		Football myFootBall = new Football("Football");

		listBall.add(myBaseBall);
		listBall.add(myFootBall);

		Address person1Address = new Address("Nguyen Trai", "HCM city", "Viet Nam", 1234);
		Address professor1Address = new Address("Nguyen Hue", "HCM city", "Viet Nam", 1234);
		Professor myProfessor1 = new Professor(55, "male", "Peter", professor1Address, 60000000);
		Subject mySubject1 = new Subject("Physic", 101, myProfessor1);
		Person myPerson1 = new Student(20, "female", "Mary", person1Address, 12, new ArrayList<Subject>() {
			{
				add(mySubject1);
			}
		}, new ArrayList<Player>() {
			{
				add(myCDPlayer);
			}
		}, new ArrayList<Ball>() {
			{
				add(myBaseBall);
			}
		});

		Address person2Address = new Address("Phan Dang Luu", "Da Nang city", "Viet Nam", 1234);
		Address professor2Address = new Address("Nguyen Dinh Chieu", "Bien Hoa city", "Viet Nam", 1234);
		Professor myProfessor2 = new Professor(55, "female", "Ran Mori", professor2Address, 50000000);
		Subject mySubject2 = new Subject("Chem", 102, myProfessor2);
		Person myPerson2 = new Student(22, "male", "Peter Pan", person2Address, 15, new ArrayList<Subject>() {
			{
				add(mySubject2);
			}
		}, new ArrayList<Player>() {
			{
				add(myDVDPlayer);
			}
		}, new ArrayList<Ball>() {
			{
				add(myBaseBall);	
		}
		});

		Address person3Address = new Address("Pham Ngoc Thach", "Hue city", "Viet Nam", 1234);
		Address professor3Address = new Address("Tran Binh Trong", "Hoi An city", "Viet Nam", 1234);
		Professor myProfessor3 = new Professor(48, "male", "Shinichi", professor3Address, 80000000);
		Subject mySubject3 = new Subject("Math", 103, myProfessor3);
		Person myPerson3 = new Student(19, "female", "Hang Le", person3Address, 16, new ArrayList<Subject>() {
			{
				add(mySubject3);
			}
		}, new ArrayList<Player>() {
			{
				add(myTapePlayer);
			}
		}, new ArrayList<Ball>() {
			{
				add(myFootBall);	
		}
		});
		
		listPlayer.add(myPerson1);
		listPlayer.add(myPerson2);
		listPlayer.add(myPerson3);

		ArrayList<Person> perListFilter = new ArrayList<Person>();
		int personPlay = Integer.parseInt(play);
		int personBallType = Integer.parseInt(ballType);
		if ((personPlay == this.CDPLAYER && personBallType == this.BASEBALL) || (personPlay == this.CDPLAYER && personBallType == this.FOOTBALL) || 
				(personPlay == this.DVDPLAYER && personBallType == this.BASEBALL) || (personPlay == this.DVDPLAYER && personBallType == this.FOOTBALL) || 
				(personPlay == this.TAPEPLAYER && personBallType == this.BASEBALL) || (personPlay == this.TAPEPLAYER && personBallType == this.FOOTBALL)) {
			for (Person person : listPlayer) {

				if (filterPerson(person.getListPlay(),person.getListBall(), personPlay,personBallType)) {
					perListFilter.add(person);
				}

			}
		} else {
			perListFilter = listPlayer;
		}

		return perListFilter;

	}

	public boolean filterPerson(ArrayList<Player> listPlay,ArrayList<Ball> listBall, int personPlay, int personBallType) {
		for (Player player : listPlay) {
			for(Ball ball: listBall) {
				if (personPlay == this.CDPLAYER && personBallType == this.BASEBALL && player.getType() == 1 && ball.BallType() == 1) {
					return true;
				}
				if (personPlay == this.CDPLAYER && personBallType == this.FOOTBALL && player.getType() == 1 && ball.BallType() == 2) {
					return true;
				}
				if (personPlay == this.DVDPLAYER && personBallType == this.BASEBALL && player.getType() == 2 && ball.BallType() == 1) {
					return true;
				}
				if (personPlay == this.DVDPLAYER && personBallType == this.FOOTBALL && player.getType() == 2 && ball.BallType() == 2) {
					return true;
				}
				if (personPlay == this.TAPEPLAYER && personBallType == this.BASEBALL && player.getType() == 3 && ball.BallType() == 1) {
					return true;
				}
				if (personPlay == this.TAPEPLAYER && personBallType == this.FOOTBALL && player.getType() == 3 && ball.BallType() == 2) {
					return true;
				}
			}
			
		}
		return false;
	}
}
