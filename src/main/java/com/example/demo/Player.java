package com.example.demo;

public interface Player {
	public String getPlay();
	public String getStop();
	public String getPause();
	public String getReverse();
	public int getType();


}
