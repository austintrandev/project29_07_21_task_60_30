package com.example.demo;

public abstract class Ball implements Tossable {
public Ball(String brandName) {
		
		this.brandName = brandName;
	}

private String brandName;
public abstract void bounce();
public abstract void toss();
public abstract int BallType();
public String getBrandName() {
	return brandName;
}
public void setBrandName(String brandName) {
	this.brandName = brandName;
}
}
