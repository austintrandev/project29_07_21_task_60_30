package com.example.demo;

import java.util.ArrayList;

public class Student extends Person {
	public Student(int age, String gender, String name, Address address,int studentId,ArrayList<Subject> listSubject, ArrayList<Player> listPlay,ArrayList<Ball>listBall) {
		super(age, gender, name, address, listPlay,listBall);
		// TODO Auto-generated constructor stub
		this.studentId = studentId;
		this.listSubject = listSubject;
	}

	
	public Student() {
		this.studentId = 123;
		this.listSubject = listSubject;
	}

	private int studentId;
	public int getStudentId() {
		return studentId;
	}


	public void setStudentId(int studentId) {
		this.studentId = studentId;
	}


	public ArrayList<Subject> getListSubject() {
		return listSubject;
	}


	public void setListSubject(ArrayList<Subject> listSubject) {
		this.listSubject = listSubject;
	}

	private ArrayList<Subject> listSubject;

	public void doHomework() {
		System.out.println("Student is doing homework!");
	}
	@Override
	public void eat() {
		// TODO Auto-generated method stub
		
	}

}
