package com.example.demo;

public class Address {
	public Address(String street, String city, String country, int postcode) {
		super();
		this.street = street;
		this.city = city;
		Country = country;
		this.postcode = postcode;
	}

	public Address() {
		this.street = "Nguyen Trai";
		this.city = "HCM city";
		Country = "Viet Nam";
		this.postcode = 1234;
	}

	private String street;
	private String city;
	private String Country;
	private int postcode;

	public boolean isValidate() {
		return true;
	}

	public String getStreet() {
		return street;
	}

	public void setStreet(String street) {
		this.street = street;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public String getCountry() {
		return Country;
	}

	public void setCountry(String country) {
		Country = country;
	}

	public int getPostcode() {
		return postcode;
	}

	public void setPostcode(int postcode) {
		this.postcode = postcode;
	}

}
